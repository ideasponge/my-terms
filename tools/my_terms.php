<?php  defined('C5_EXECUTE') or die(_("Access Denied."));

/******************************************************************************************************************
Author Jason Pender ideasponge.com  see concrete5.org for license info
******************************************************************************************************************/
$db = Loader::db();
$row = $db->GetRow('select modalText from atMyTermsCheckboxBooleanSettings where akID = ?', $_GET['ak']);
echo $row['modalText'];
exit;