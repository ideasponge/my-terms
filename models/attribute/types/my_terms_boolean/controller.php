<?php   defined('C5_EXECUTE') or die(_("Access Denied."));

class MyTermsBooleanAttributeTypeController extends BooleanAttributeTypeController  {

	public function getValue() {
		$db = Loader::db();
		$value = $db->GetOne("select value from atMyTermsCheckboxBoolean where avID = ?", array($this->getAttributeValueID()));
		return $value;	
	}
	
	protected function load() {
		$ak = $this->getAttributeKey();
		if (!is_object($ak)) {
			return false;
		}
		
		$db = Loader::db();
		$row = $db->GetRow('select akCheckedByDefault, linkTitle, modalText from atMyTermsCheckboxBooleanSettings where akID = ?', $ak->getAttributeKeyID());
		
		$this->akCheckedByDefault = $row['akCheckedByDefault'];
		$this->linkTitle = $row['linkTitle'];
		$this->modalText = $row['modalText'];
		
		$this->set('linkTitle',$this->linkTitle);
		$this->set('modalText',$this->modalText);
		$this->set('akCheckedByDefault', $this->akCheckedByDefault);
	}
	

	public function form() {
		$html = Loader::helper('html');
		 $this->addHeaderItem($html->css('jquery.ui.css'));
		 $this->addHeaderItem($html->css('ccm.dialog.css'));
		 $this->addHeaderItem($html->javascript('jquery.ui.js'));
		 $this->addHeaderItem($html->javascript('ccm.dialog.js'));
		//parent::form();
		if (is_object($this->attributeValue)) {
         $value = $this->getAttributeValue()->getValue();
         $checked = $value == 1 ? true : false;
		} else {
		   $this->load();
		   if ($this->akCheckedByDefault) {
			  $checked = true;
		   }
		}
		$ak = $this->getAttributeKey();
		$cb = Loader::helper('form')->checkbox($this->field('value'), 1, $checked);
		print '<label>' . $cb;
		$ak = $this->getAttributeKey();
		if(strlen($this->linkTitle) > 0) { ?>
			 <a dialog-title="<?php echo $ak->getAttributeKeyName() ?>" 
             dialog-modal="false" 
             id="myTermsDialog<?php echo $ak->getAttributeKeyID(); ?>" 
             href="<?php echo Loader::helper('concrete/urls')->getToolsURL('my_terms', 'my_terms'); ?>?ak=<?php echo $ak->getAttributeKeyID(); ?>"><?php echo $this->linkTitle; ?></a></label>
             <script type="text/javascript">
				 $(function() {
					$("#myTermsDialog<?php echo $ak->getAttributeKeyID(); ?>").dialog();
				 });
			  </script>
		<?php }
	}

	public function type_form() {

		$form = Loader::helper('form');
		$this->load();
		?>
        <fieldset>
            <legend><?php  echo t('Additional Options')?></legend>
            <div class="clearfix">
            <label><?php  echo t('Checked By Default')?></label>
            <div class="input">
            <ul class="inputs-list">
                <li><label><?php  echo $form->checkbox('akCheckedByDefault', 1, $this->akCheckedByDefault)?></label></li>
            </ul>
            </div>
            </div>
            
            <div class="clearfix">
            <label><?php  echo t('Link Text')?></label>
            <div class="input">
            <ul class="inputs-list">
                <li><label><?php  echo $form->text('linkTitle', $this->linkTitle)?></label></li>
            </ul>
            </div>
            </div>
            
            
            <div class="clearfix">
            <label><?php  echo t('My Terms Content')?></label>
            <div class="input">
            <ul class="inputs-list">
                <li><?php   Loader::element('editor_init'); ?>
                    <?php   Loader::element('editor_config'); ?>
                    <?php   Loader::element('editor_controls', array('mode'=>'full')); ?>
                    <?php  echo $form->textarea('modalText', $this->modalText, array('style' => 'height: 50px; width: 100%', 'class' => 'ccm-advanced-editor'))?></li>

            </ul>
            </div>
            </div>
        </fieldset>
		<?php  		
	}
	

	public function saveValue($value) {
		$db = Loader::db();
		$value = ($value == false || $value == '0') ? 0 : 1;
		$db->Replace('atMyTermsCheckboxBoolean', array('avID' => $this->getAttributeValueID(), 'value' => $value), 'avID', true);
	}
	

	public function deleteKey() {
		$db = Loader::db();
		$db->Execute('delete from atMyTermsCheckboxBooleanSettings where akID = ?', array($this->getAttributeKey()->getAttributeKeyID()));

		$arr = $this->attributeKey->getAttributeValueIDList();
		foreach($arr as $id) {
			$db->Execute('delete from atMyTermsCheckboxBoolean where avID = ?', array($id));
		}
	}
	

	public function duplicateKey($newAK) {
		$this->load();
		$db = Loader::db();
		$db->Execute('insert into atMyTermsCheckboxBooleanSettings (akID, akCheckedByDefault) values (?, ?)', array($newAK->getAttributeKeyID(), $this->akCheckedByDefault));	
	}

	public function saveKey($data) {
		$ak = $this->getAttributeKey();
		$db = Loader::db();
		$akCheckedByDefault = $data['akCheckedByDefault'];
		$linkTitle = $data['linkTitle'];
		$modalText = $data['modalText'];
		
		if ($data['akCheckedByDefault'] != 1) {
			$akCheckedByDefault = 0;
		}

		$db->Replace('atMyTermsCheckboxBooleanSettings', array(
			'akID' => $ak->getAttributeKeyID(), 
			'akCheckedByDefault' => $akCheckedByDefault,
			'linkTitle' => $linkTitle,
			'modalText' => $modalText
		), array('akID'), true);
	}
	
	
	public function validateForm($data) {
		return $data['value'] == 1;
	}
	

	public function deleteValue() {
		$db = Loader::db();
		$db->Execute('delete from atMyTermsCheckboxBoolean where avID = ?', array($this->getAttributeValueID()));
	}
	
}
