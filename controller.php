<?php   

defined('C5_EXECUTE') or die(_("Access Denied."));

class MyTermsPackage extends Package {

	protected $pkgHandle = 'my_terms';
	protected $appVersionRequired = '5.6.1';
	protected $pkgVersion = '1.0';
											
	
	public function getPackageDescription() {
		return t('Attribute for handling site terms and other agreements.');
	}
	
	public function getPackageName() {
		return t('My Terms');
	}
	
	public function install() {
		
		$pkg = parent::install();

		// install attribute categories
		$eaku = AttributeKeyCategory::getByHandle('user');
		$atPpAdjustB = AttributeType::add('my_terms_boolean', t('Checkbox - My Terms'), $pkg);
		$eaku->associateAttributeKeyType(AttributeType::getByHandle('my_terms_boolean'));

		
	}
	
	public function upgrade() {


	}
	
	public function uninstall() {
		parent::uninstall();
		
			$db = Loader::db();
			// may want to make a backup first -JP
			$db->Execute('DROP TABLE If EXISTS atMyTermsCheckboxBoolean, atMyTermsCheckboxBooleanSettings');


	}
	

}